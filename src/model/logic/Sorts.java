package model.logic;

import java.util.Comparator;

import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;

public class Sorts <T extends Comparable>
{
	//------------------------------
	// QuickSort
	//------------------------------

	private int partition(T arr[], int inicio, int fin) 
	{ 
		T pivot = arr[fin];  
		int i = (inicio-1);
		for (int j=inicio; j < fin; j++) 
		{  
			if (arr[j].compareTo(pivot) < 0 ) 
			{ 
				i++; 
				T temp = arr[i]; 
				arr[i] = arr[j]; 
				arr[j] = temp; 
			} 
		} 
		T temp = arr[i+1]; 
		arr[i+1] = arr[fin]; 
		arr[fin] = temp; 

		return i+1; 
	} 

	public long quickSort(T arr[], int inicio, int fin) 
	{ 
		if (inicio < fin) 
		{ 
			int p = partition (arr, inicio, fin); 

			quickSort(arr, inicio, p-1);
			quickSort(arr, p+1, fin);
		}
		return System.currentTimeMillis();
	}

	public void quick3way (DoublyLinkedList arr, Node inicio, Node fin, int start, int end, boolean ascendente)
	{
		if (start < end)
		{
			Node izq = inicio;
			Node i = inicio.darSiguiente();
			Node der = fin;
			int ii = start + 1;
			int ider = end;
			int iizq = start;
			Comparable v = (Comparable) inicio.darElemento();
			while (ii <= ider)
			{
				if (ascendente)
				{
					if (((Comparable) i.darElemento()).compareTo(v) < 0)
					{
						T temp = (T) izq.darElemento();
						izq.cambiarElemento(i.darElemento());
						i.cambiarElemento(temp);
						izq = izq.darSiguiente();
						i = i.darSiguiente();
						ii++;
						iizq++;
					}
					else if (((Comparable) i.darElemento()).compareTo(v) > 0)
					{
						T temp = (T) i.darElemento();
						i.cambiarElemento(der.darElemento());
						der.cambiarElemento(temp);
						ider--;
						der = der.darAnterior();
					}
					else
					{
						ii++;
						i = i.darSiguiente();
					}
				}
				else
				{
					if (((Comparable) i.darElemento()).compareTo(v) > 0)
					{
						T temp = (T) izq.darElemento();
						izq.cambiarElemento(i.darElemento());
						i.cambiarElemento(temp);
						izq = izq.darSiguiente();
						i = i.darSiguiente();
						ii++;
						iizq++;
					}
					else if (((Comparable) i.darElemento()).compareTo(v) < 0)
					{
						T temp = (T) i.darElemento();
						i.cambiarElemento(der.darElemento());
						der.cambiarElemento(temp);
						ider--;
						der = der.darAnterior();
					}
					else
					{
						ii++;
						i = i.darSiguiente();
					}
				}
			}
			quick3way(arr, inicio, izq.darAnterior(), start, iizq - 1, ascendente);
			quick3way(arr, der.darSiguiente(), fin, ider + 1, end, ascendente);
		}
	}

	/**
	 * @param arr
	 * @param inicio
	 * @param fin
	 * @param start
	 * @param end
	 * @param ascendente
	 */
	public void quick3way (DoublyLinkedList <T> arr, Node inicio, Node fin, int start, int end, boolean ascendente, Comparator <T> comparador)
	{
		if (start < end)
		{
			Node izq = inicio;
			Node i = inicio.darSiguiente();
			Node der = fin;
			int ii = start + 1;
			int ider = end;
			int iizq = start;
			T v = (T) inicio.darElemento();
			while (ii <= ider)
			{
				if (ascendente)
				{
					if ( comparador.compare((T) i.darElemento(), v) < 0)
					{
						T temp = (T) izq.darElemento();
						izq.cambiarElemento(i.darElemento());
						i.cambiarElemento(temp);
						izq = izq.darSiguiente();
						i = i.darSiguiente();
						ii++;
						iizq++;
					}
					else if (comparador.compare((T) i.darElemento(), v) > 0)
					{
						T temp = (T) i.darElemento();
						i.cambiarElemento(der.darElemento());
						der.cambiarElemento(temp);
						ider--;
						der = der.darAnterior();
					}
					else
					{
						ii++;
						i = i.darSiguiente();
					}
				}
				else
				{
					if (comparador.compare((T) i.darElemento(), v) > 0)
					{
						T temp = (T) izq.darElemento();
						izq.cambiarElemento(i.darElemento());
						i.cambiarElemento(temp);
						izq = izq.darSiguiente();
						i = i.darSiguiente();
						ii++;
						iizq++;
					}
					else if (comparador.compare((T) i.darElemento(), v) < 0)
					{
						T temp = (T) i.darElemento();
						i.cambiarElemento(der.darElemento());
						der.cambiarElemento(temp);
						ider--;
						der = der.darAnterior();
					}
					else
					{
						ii++;
						i = i.darSiguiente();
					}
				}
			}
			quick3way(arr, inicio, izq.darAnterior(), start, iizq - 1, ascendente, comparador);
			quick3way(arr, der.darSiguiente(), fin, ider + 1, end, ascendente, comparador);
		}
	}

	//------------------------------
	// MergeSort
	//------------------------------

	private void merge(T vectorAOrdenar[], int inicio, int mitad, int fin) 
	{ 
		//guardar variables temporales con la informacion de los tama�os 
		// util para evitar realizar multiples veces la misma cuenta
		int size1 = mitad - inicio + 1; 
		int size2 = fin - mitad; 

		Object primero[] = new Object [size1]; 
		Object segundo[] = new Object [size2]; 

		for (int i=0; i<size1; ++i) 
		{
			primero[i] = vectorAOrdenar[inicio + i]; 
		}
		for (int j=0; j<size2; ++j) 
		{
			segundo[j] = vectorAOrdenar[mitad + 1+ j]; 
		}

		int i =0;
		int j = 0; 


		int k = inicio; 
		while (i < size1 && j < size2) 
		{ 
			T elemento1 = (T)primero[i];
			T elemento2 = (T)segundo[j];

			if (elemento1.compareTo(elemento2)<0) 
			{ 
				vectorAOrdenar[k] = elemento1; 
				i++; 
			} 
			else
			{ 
				vectorAOrdenar[k] = elemento2; 
				j++; 
			} 
			k++; 
		} 

		while (i < size1) 
		{ 
			vectorAOrdenar[k] = (T)primero[i]; 
			i++; 
			k++; 
		} 

		while (j < size2) 
		{ 
			vectorAOrdenar[k] = (T)segundo[j]; 
			j++; 
			k++; 
		} 
	}

	public long mergeSort(T vectorAOrdenar[], int inicio, int fin) 
	{ 
		if (inicio < fin) 
		{ 

			int mitad = (inicio+fin)/2; 
			mergeSort(vectorAOrdenar, inicio, mitad); 
			mergeSort(vectorAOrdenar , mitad+1, fin); 
			merge(vectorAOrdenar, inicio, mitad, fin); 
		} 
		return System.currentTimeMillis(); 
	} 

}