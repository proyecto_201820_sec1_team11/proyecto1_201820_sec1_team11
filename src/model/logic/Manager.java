package model.logic;

import model.data_structures.DoublyLinkedList;
import model.data_structures.ICola;
import model.data_structures.ILista;
import model.data_structures.Iterador;
import model.data_structures.Node;
import model.data_structures.Cola;
import model.vo.Bike;
import model.vo.ComparadorDistancia;
import model.vo.ComparadorFechas;
import model.vo.ComparadorViajes;
import model.vo.Station;
import model.vo.Trip;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

import api.IManager;

public class Manager <T> implements IManager
{
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "data" + File.separator + "Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "data" + File.separator + "Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "data" + File.separator + "Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "data" + File.separator + "Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "data" + File.separator + "Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "data" + File.separator + "Divvy_Stations_2017_Q3Q4.csv";

	// Verifica si ya se cargaron estos archivos para no volver a cargarlos

	private boolean q1 = false;

	private boolean q2 = false;

	private boolean q3 = false;

	private boolean q4 = false;

	private boolean stationsq1q2 = false;

	private boolean stationsq3q4 = false;

	private final static int totalStationsQ1Q2 = 582;

	private final static int totalStationsQ3Q4 = 585; 

	// Estructuras donde se cargan los archivos
	private DoublyLinkedList <Trip> trips = new DoublyLinkedList <Trip>();

	private Station [] stations;

	// M�todos auxiliares de carga

	public void loadStations (String stationsFile)
	{
		try 
		{
			FileReader lector = new FileReader(stationsFile);
			BufferedReader reader = new BufferedReader (lector);
			reader.readLine();
			String l = reader.readLine();
			int i = 0;
			while (l != null)
			{
				String [] linea = l.split(",");
				int id = Integer.parseInt(linea[0].charAt(0) == '"'? linea[0].substring(1, linea[0].length()-1) : linea[0]);
				String nombre = linea[1].charAt(0) == '"'? linea[1].substring(1, linea[1].length()-1) : linea[1];
				double latitud = Double.parseDouble(linea[3].charAt(0) == '"'? linea[3].substring(1, linea[3].length()-1) : linea[3]);
				double longitud = Double.parseDouble(linea[4].charAt(0) == '"'? linea[4].substring(1, linea[4].length()-1) : linea[4]);
				int capacidad = Integer.parseInt(linea[5].charAt(0) == '"'? linea[5].substring(1, linea[5].length()-1) : linea[5]);
				String [] date = (linea[6].charAt(0) == '"'? linea[6].substring(1, linea[6].length()-1) : linea[6]).split(" ");
				LocalDateTime fecha = convertirFecha_Hora_LDT(date [0], date[1]);

				Station station = new Station(id, nombre, latitud, longitud, capacidad, fecha);
				stations [i] = station;
				l = reader.readLine();
				i++;
			}
			reader.close();
			lector.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public DoublyLinkedList <Trip> loadTrips (String tripsFile)
	{
		DoublyLinkedList <Trip> temp = new DoublyLinkedList <Trip> ();
		try 
		{
			FileReader lector = new FileReader(tripsFile);
			BufferedReader reader = new BufferedReader (lector);
			reader.readLine();
			String l = reader.readLine();
			while (l != null  )
			{
				String [] linea = l.split(",");
				int tripId = Integer.parseInt(linea[0].charAt(0) == '"'? linea[0].substring(1, linea[0].length()-1) : linea[0]);
				String [] ini = (linea [1].charAt(0) == '"'? linea[1].substring(1, linea[1].length()-1) : linea[1]).split(" ");
				String [] fin = (linea [2].charAt(0) == '"'? linea[2].substring(1, linea[2].length()-1) : linea[2]).split(" ");
				LocalDateTime start = convertirFecha_Hora_LDT(ini [0], ini[1]);
				LocalDateTime end = convertirFecha_Hora_LDT(fin[0], fin[1]);
				int bikeId = Integer.parseInt(linea [3].charAt(0) == '"'? linea[3].substring(1, linea[3].length()-1) : linea[3]);
				Double duration = Double.parseDouble(linea [4].charAt(0) == '"'? linea[4].substring(1, linea[4].length()-1) : linea[4]);
				int fromStId = Integer.parseInt(linea [5].charAt(0) == '"'? linea[5].substring(1, linea[5].length()-1) : linea[5]);
				String fromSt = linea [6].charAt(0) == '"'? linea[6].substring(1, linea[6].length()-1) : linea[6];
				int toStId = Integer.parseInt(linea [7].charAt(0) == '"'? linea[7].substring(1, linea[7].length()-1) : linea[7]);
				String toSt = linea [8].charAt(0) == '"'? linea[8].substring(1, linea[8].length()-1) : linea[8];
				String tipo = linea [9].equals("\"Dependent\"")? "Subscriber" : linea[9].charAt(0) == '"'? linea [9].substring(1, linea[9].length()-1) : linea[9];
				String genero = null;
				int year = 0;
				if (linea[10].length() > 2)
					genero = linea [10].substring(1, linea[10].length()-1);
				if (linea[11].length() > 2)
					year = Integer.parseInt(linea [11].substring(1, linea[11].length()-1));

				Trip viaje = new Trip(tripId, start, end, bikeId, duration, fromStId, fromSt, toStId, toSt, tipo, genero, year);
				temp.add(viaje);
				l = reader.readLine();
			}
			reader.close();
			lector.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return temp;
	}

	// PARTE A: DANIEL PEREZ

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ICola <Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	/**
	 * COMPLEJIDAD: O((N^2)+(N-1))
	 */
	public ILista <Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ILista <Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ILista <Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}

	// PARTE B: JUAN J TORRES

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ICola <Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo)
	{
		Cola <Station> cola = new Cola <Station> ();
		for (int i = 0; i < stations.length; i++)
		{
			if (stations[i].getOnline_date().compareTo(fechaComienzo) > 0)
				cola.enqueue(stations[i]);
		}
		return cola;
	}

	/**
	 * COMPLEJIDAD: O((N^2)+N)
	 */
	public ILista <Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		DoublyLinkedList <Trip> lista = new DoublyLinkedList <Trip> ();
		Iterator <Trip> iter = trips.iterator();
		while (iter.hasNext())
		{
			Trip temp = iter.next();
			if (temp.getStartTime().compareTo(fechaFinal) > 0)
				break;
			if (temp.getStartTime().compareTo(fechaInicial) >= 0)
				lista.addAtEnd(temp);
		}

		DoublyLinkedList <Bike> bikes = new DoublyLinkedList <Bike> ();

		Sorts ord = new Sorts ();
		ord.quick3way(lista, lista.getFirst(), lista.getLast(), 0, lista.size() - 1, true);
		int distancia = 0;
		int viajes = 0;
		Node <Trip> actual = lista.getFirst();
		while (actual != null)
		{
			if (actual.darSiguiente() == null)
			{
				Station inicio = searchStation(actual.darElemento().getFromStationId());
				Station fin = searchStation(actual.darElemento().getToStationId());
				distancia += distancia(inicio.getLatitude(), inicio.getLongitude(), fin.getLatitude(), fin.getLongitude());
				viajes ++;
				Bike bici = new Bike(actual.darElemento().getBikeId(), viajes, distancia, 0);
				bikes.add(bici);
				distancia = 0;
				viajes = 0;
				actual = actual.darSiguiente();
			}
			else
			{
				if (actual.darElemento().getBikeId() == actual.darSiguiente().darElemento().getBikeId())
				{
					Station inicio = searchStation(actual.darElemento().getFromStationId());
					Station fin = searchStation(actual.darElemento().getToStationId());
					distancia += distancia(inicio.getLatitude(), inicio.getLongitude(), fin.getLatitude(), fin.getLongitude());
					viajes ++;
					actual = actual.darSiguiente();
				}
				else
				{
					Station inicio = searchStation(actual.darElemento().getFromStationId());
					Station fin = searchStation(actual.darElemento().getToStationId());
					distancia += distancia(inicio.getLatitude(), inicio.getLongitude(), fin.getLatitude(), fin.getLongitude());
					viajes ++;
					Bike bici = new Bike(actual.darElemento().getBikeId(), viajes, distancia, 0);
					bikes.add(bici);
					distancia = 0;
					viajes = 0;
					actual = actual.darSiguiente();
				}
			}
		}
		ord.quick3way(bikes, bikes.getFirst(), bikes.getLast(), 0, bikes.size() - 1, false, new ComparadorDistancia());
		return bikes;
	}

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ILista <Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero)
	{
		DoublyLinkedList <Trip> lista = new DoublyLinkedList <Trip> ();
		Iterator <Trip> iter = trips.iterator();
		while (iter.hasNext())
		{
			Trip temp = iter.next();
			if (temp.getBikeId() == bikeId && temp.getTripDuration() < tiempoMaximo && temp.getGender().equals(Trip.Genero.valueOf(genero)))
				lista.addAtEnd(temp);
		}
		return lista;
	}

	/**
	 *  COMPLEJIDAD: O(N)
	 */
	public ILista <Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		DoublyLinkedList <Trip> lista = new DoublyLinkedList <Trip> ();
		Iterator <Trip> iter = trips.iterator();
		while (iter.hasNext())
		{
			Trip temp = iter.next();
			if (temp.getStartTime().compareTo(fechaFinal) > 0)
				break;
			if (temp.getFromStationId() == startStationId && temp.getStartTime().compareTo(fechaInicial) >= 0)
				lista.addAtEnd(temp);
		}
		return lista;
	}

	// PARTE C

	/**
	 * Pre: Siempre se carga de forma incremental.
	 * COMPLEJIDAD: O(N)
	 */
	public int []  C1cargar(String rutaTrips, String rutaStations)
	{
		if (rutaTrips.equals(""))
		{
			trips.clear();
			stations = new Station [0];
		}
		else
		{
			if (rutaTrips.equals(TRIPS_Q1) && !q1)
			{
				q1 = true;
				stationsq1q2 = true;
				stations = new Station [totalStationsQ1Q2];
				trips = loadTrips(rutaTrips);
				loadStations(rutaStations);
				Sorts sort = new Sorts();
				sort.mergeSort(stations, 0, stations.length-1);
			}
			else if (rutaTrips.equals(TRIPS_Q2) && !q2  && q1)
			{
				q2 = true;
				trips.concat(loadTrips(rutaTrips));
			}
			else if (rutaTrips.equals(TRIPS_Q3) && !q3 && q2)
			{
				q3 = true;
				stationsq3q4 = true;
				trips.concat(loadTrips(rutaTrips));
				stations = new Station [totalStationsQ3Q4];
				loadStations(rutaStations);
			}
			else if (rutaTrips.equals(TRIPS_Q4) && !q4 && q3)
			{
				q4 = true;
				trips.concat(loadTrips(rutaTrips));
			}
		}
		int [] i = {trips.size(), stations.length};
		return i;
	}

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ICola <Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		return null;
	}

	/**
	 * COMPLEJIDAD: O((N^2)+N)
	 */
	public ILista <Bike> C3BicicletasMasUsadas(int topBicicletas)
	{
		DoublyLinkedList <Bike> bikes = new DoublyLinkedList <Bike> ();
		Sorts ord = new Sorts ();
		ord.quick3way(trips, trips.getFirst(), trips.getLast(), 0, trips.size() - 1, true);
		int viajes = 0;
		int i = 0;
		Node <Trip> actual = trips.getFirst();
		while (actual != null)
		{
			if (actual.darSiguiente() == null)
			{
				viajes ++;
				Bike bici = new Bike(actual.darElemento().getBikeId(), viajes, 0, 0);
				bikes.add(bici);
				viajes = 0;
				actual = actual.darSiguiente();
			}
			else
			{
				if (actual.darElemento().getBikeId() == actual.darSiguiente().darElemento().getBikeId())
				{
					viajes ++;
					actual = actual.darSiguiente();
				}
				else
				{
					viajes ++;
					Bike bici = new Bike(actual.darElemento().getBikeId(), viajes, 0, 0);
					bikes.add(bici);
					viajes = 0;
					actual = actual.darSiguiente();
				}
			}
		}
		ord.quick3way(bikes, bikes.getFirst(), bikes.getLast(), 0, bikes.size() - 1, false, new ComparadorViajes());
		DoublyLinkedList temp = new DoublyLinkedList();
		Iterator iter = bikes.iterator();
		for (int j = 0; j < topBicicletas && iter.hasNext(); j++)
		{
			temp.addAtEnd(iter.next());
		}
		return temp;
	}

	/**
	 * COMPLEJIDAD: O(N)
	 */
	public ILista <Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal)
	{
		DoublyLinkedList inicio = new DoublyLinkedList ();
		DoublyLinkedList fin = new DoublyLinkedList ();
		Iterator <Trip >iter = trips.iterator();
		while (iter.hasNext())
		{
			Trip temp = iter.next();
			if (temp.getFromStationId() == StationId)
				inicio.addAtEnd(temp);
			else if (temp.getToStationId() == StationId)
			{
				temp.setFromStationName(null);
				fin.addAtEnd(temp);
			}
		}

		Sorts ord = new Sorts ();
		ord.quick3way(fin, fin.getFirst(), fin.getLast(), 0, fin.size() - 1, true, new ComparadorFechas());
		
		return null;
	}

	// EXTENSI�N

	private static final int RADIO_TIERRA = 6371;

	private static double distancia (double startLat, double startLong, double endLat, double endLong) {

		double dLat  = Math.toRadians((endLat - startLat));
		double dLong = Math.toRadians((endLong - startLong));

		startLat = Math.toRadians(startLat);
		endLat   = Math.toRadians(endLat);

		double a = haversin(dLat) + Math.cos(startLat) * Math.cos(endLat) * haversin(dLong);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return RADIO_TIERRA * c;
	}

	private static double haversin(double val)
	{
		return Math.pow(Math.sin(val / 2), 2);
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		if (datosHora.length > 2)
		{
			int segundos = Integer.parseInt(datosHora[2]);
			return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
		}
		else
			return LocalDateTime.of(agno, mes, dia, horas, minutos);
	}

	private Station searchStation (int id)
	{
		int inicio = 0;
		int fin = stations.length-1;
		boolean encontrado = false;
		Station est = null;
		while (!encontrado)
		{
			int mid = (inicio + fin)/2;
			if (id == stations[mid].getId())
			{
				encontrado = true;
				est = stations[mid];
			}
			else if (id < stations[mid].getId())
				fin = mid-1;
			else
				inicio = mid+1;
		}
		return est;
	}
}