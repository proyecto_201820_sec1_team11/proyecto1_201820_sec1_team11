package model.data_structures;

import java.io.Serializable;
import java.util.ListIterator;

/**
 * Clase que representa el iterador de lista (avanza hacia adelante y hacia atrás)
 * @param <E> Tipo del objeto que almacena el iterador de la lista
 */
public class ListIterador <E> implements ListIterator<E>, Serializable 
{
	/**
	 * Constante de serialización
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private Node <E> anterior;

	/**
	 * Nodo anterior al que se encuentra el iterador.
	 */
	private Node <E> siguiente;

	/**
	 * Nodo en el que se encuentra el iterador.
	 */
	private Node <E> actual;

	/**
	 * Crea un nuevo iterador de lista iniciando en el nodo que llega por parámetro
	 * @param primerNodo el nodo en el que inicia el iterador. nActual != null
	 */
	public ListIterador (Node <E> primerNodo)
	{
		actual = primerNodo;
	}

	/**
	 * Indica si hay nodo siguiente
	 * true en caso que haya nodo siguiente o false en caso contrario
	 */
	public boolean hasNext() 
	{
		return this.actual != null;
	}

	/**
	 * Indica si hay nodo anterior
	 * true en caso que haya nodo anterior o false en caso contrario
	 */
	public boolean hasPrevious() 
	{
		return this.anterior != null;
	}

	/**
	 *retorna el nodo actual	
	 */
	public Node<E> getActual() 
	{
		return actual;
	}
	/**
	 * Devuelve el elemento siguiente de la iteración y avanza.
	 * @return elemento siguiente de la iteración
	 */
	public E next() 
	{
		E valor = actual.darElemento();
		anterior = actual;
		actual = (Node <E>) actual.darSiguiente();

		return valor;
	}

	/**
	 * Devuelve el elemento anterior de la iteración y retrocede.
	 * @return elemento anterior de la iteración.
	 */
	public E previous() 
	{
		E valor = actual.darElemento();		
		siguiente = actual;
		actual = anterior;
		

		return valor;
	}

	//=======================================================
	// Métodos que no se implementarán
	//=======================================================

	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}

	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}
}