package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <E>
 */
public interface ILista <E> extends Iterable<E>
{
	public boolean addAtEnd(E elem);

	public void addAtK (int i, E elem);

	public E getElement(int i);

	public int size();

	public boolean remove(E elem);

	public E removeAtK(int i);

	public boolean add(E e);

	Node<E> getFirst();
}