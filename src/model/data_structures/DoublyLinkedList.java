package model.data_structures;

import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class DoublyLinkedList <E extends Comparable> implements ILista
{
	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	private int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	private Node <E> primerNodo;

	private Node<E> ultimoNodo;

	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public DoublyLinkedList () 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public DoublyLinkedList (E nPrimero)
	{
		if(nPrimero == null)
			throw new NullPointerException("Se recibe un elemento nulo");

		primerNodo = new Node <E>(nPrimero);
		cantidadElementos = 1;
	}

	/**
	 * Indica el tama�o de la lista.
	 * @return La cantidad de nodos de la lista.
	 */
	public int size() 
	{
		return cantidadElementos;
	}

	/**
	 * Indica si la lista est� vacia
	 * @return true si la lista est� vacia o false en caso contrario
	 */
	public boolean isEmpty() 
	{
		return primerNodo == null;
	}

	/**
	 * Borra todos los elementos de la lista. Actualiza la cantidad de elementos en 0
	 * <b>post:</b> No hay elementos en la lista
	 */
	public void clear() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public Iterator<E> iterator() 
	{
		return new Iterador <E> (primerNodo);
	}

	public ListIterator <E> listIterador()
	{
		return new ListIterador <E> (primerNodo);
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	public boolean remove(Object o) 
	{
		boolean eliminado = false;
		boolean encontrado = false;
		Node<E> actual = (Node<E>) primerNodo;
		while (actual!= null && !encontrado)
		{
			if (actual.darElemento().equals(o))
			{
				if (actual.darAnterior() != null)
					actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				else primerNodo = (Node<E>) actual.darSiguiente();
				if (actual.darSiguiente() != null)
				{
					Node<E> siguiente = (Node<E>) actual.darSiguiente();
					siguiente.cambiarAnterior(actual.darAnterior());
				}
				encontrado = true;
				eliminado = true;
				cantidadElementos --;
			}
			else 
			{
				actual = (Node<E>) actual.darSiguiente();
			}
		}
		return eliminado;
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E removeAtK (int index) 
	{
		E eliminado = null;
		if (!isEmpty())
		{		
			if (index < 0 || index >= cantidadElementos)
				throw new IndexOutOfBoundsException();

			Node<E> actual = (Node<E>) primerNodo;
			int posActual = 0;
			while( posActual < (index))
			{
				posActual ++;
				actual = (Node<E>) actual.darSiguiente();
			}
			eliminado = actual.darElemento();
			if (actual.darAnterior() != null)
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente().darSiguiente());
			else primerNodo = (Node<E>) actual.darSiguiente();
			if (actual.darSiguiente() != null)
			{
				Node<E> siguiente = (Node<E>) actual.darSiguiente();
				siguiente.cambiarAnterior(actual.darAnterior());
			}
			cantidadElementos --;
		}
		return eliminado;
	}

	@Override
	public Node<E> getFirst()
	{
		return primerNodo;	
	}

	public Node <E> getLast()
	{
		return ultimoNodo;		
	}

	public Node <E> get (int index)
	{
		if(index < 0 || index >= cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}
		Node <E> actual = null;
		if ((index-0) <= (cantidadElementos-1-index))
		{
			actual = primerNodo;
			for (int i = 0; i <= index-0; i++)
				actual = actual.darSiguiente();
		}
		else
		{
			actual = ultimoNodo;
			for (int i = 0; i <= cantidadElementos-1-index; i++)
				actual = actual.darAnterior();
		}
		return actual;
	}

	/**
	 * Devuelve el nodo de la posici�n dada
	 * @param pos la posici�n  buscada
	 * @return el nodo en la posici�n dada 
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E getElement (int index)
	{
		if(index < 0 || index >= cantidadElementos)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + index + " y el tama�o de la lista es de " + cantidadElementos);
		}

		Node <E> actual = primerNodo;
		int posActual = 0;
		while(actual != null && posActual < index)
		{
			actual = actual.darSiguiente();
			posActual ++;
		}
		return actual.darElemento();
	}


	public void concat (DoublyLinkedList <E> lista)
	{
		lista.getFirst().cambiarAnterior(ultimoNodo);
		ultimoNodo.cambiarSiguiente(lista.getFirst());
		ultimoNodo = lista.getLast();
		cantidadElementos += lista.size();
	}

	/**
	 * Agrega un elemento al inicio de la lista
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * Se actualiza la cantidad de elementos.
	 * @param e el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	@Override
	public boolean add(Object e)
	{
		boolean a�adido = false;
		if (e != null)
		{
			Node nuevo = new Node ((E) e);
			if (primerNodo == null)
			{
				primerNodo = nuevo;
				ultimoNodo = nuevo;
				a�adido = true;
				cantidadElementos ++;
			}
			else
			{
				primerNodo.cambiarAnterior(nuevo);
				nuevo.cambiarSiguiente(primerNodo);
				primerNodo = nuevo;
				a�adido = true;
				cantidadElementos ++;
			}
		}
		else throw new NullPointerException();
		return a�adido;
	}

	@Override
	public boolean addAtEnd(Object e)
	{
		boolean a�adido = false;

		if (e != null)
		{
			Node <E> nuevo = new Node <E>((E) e);
			if(isEmpty())
			{
				primerNodo = nuevo;
				ultimoNodo = nuevo;
				a�adido = true;
				cantidadElementos ++;
			}
			else
			{
				ultimoNodo.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(ultimoNodo);
				ultimoNodo = nuevo;
				a�adido = true;
				cantidadElementos ++;  
			}
		}
		else throw new NullPointerException();

		return a�adido;
	}

	/**
	 * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan.
	 * Actualiza la cantidad de elementos.
	 * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�o de la lista se agrega al final
	 * @param elem el elemento que se desea agregar
	 * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
	 * @throws NullPointerException Si el elemento que se quiere agregar es null.
	 */
	@Override
	public void addAtK(int index, Object elemento)
	{
		if (index < 0 || index > cantidadElementos)
			throw new IndexOutOfBoundsException();

		if (elemento != null)
		{
			if (isEmpty())
			{
				primerNodo = new Node <E>((E) elemento);
				cantidadElementos ++;
			}
			else
			{
				Node <E> nuevo = new Node <E>((E) elemento);
				if (index == 0)
					add(elemento);

				else if (index == cantidadElementos)
					addAtEnd(elemento);

				else 
				{
					Node <E> n = primerNodo;
					int posActual = 0;
					while( posActual < (index-1))
					{
						posActual++;
						n = n.darSiguiente();
					}
					nuevo.cambiarSiguiente(n.darSiguiente());
					nuevo.cambiarAnterior(n);
					n.cambiarSiguiente(nuevo);
					cantidadElementos ++;
				}
			}
		}
		else throw new NullPointerException();
	}
}