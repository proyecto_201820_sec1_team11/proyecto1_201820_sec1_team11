package model.data_structures;

import java.util.Iterator;

public class Pila <T> implements IPila <T>
{
	/**
	 * nodo del elemento que fue el �ltimo en ingresar
	 */
	private Node<T> ultimoNodo;


	private int size;

	public Pila ()
	{
		ultimoNodo = new Node<T> (null);
	}

	public Pila (T elem)
	{
		ultimoNodo = new Node<T> (elem);
		size = 1;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Iterador<T> (ultimoNodo);
	}

	@Override
	public boolean isEmpty()
	{
		return size == 0;
	}

	@Override
	public int size()
	{
		return size;
	}

	public Node darUltimo ()
	{
		return ultimoNodo;
	}

	@Override
	public void push(T t)
	{
		if (t != null)
		{
			if (isEmpty())
			{
				ultimoNodo = new Node <T> (t);
				size = 1;
			}
			else
			{
				Node <T> nuevo = new Node <T> (t);
				ultimoNodo.cambiarAnterior(nuevo);
				nuevo.cambiarSiguiente(ultimoNodo);
				ultimoNodo = nuevo;
				size ++;
			}
		}
	}

	@Override
	public T pop()
	{
		if (isEmpty())
			return null;
		else if (size == 1)
		{
			T temp = ultimoNodo.darElemento();
			ultimoNodo = null;
			size = 0;
			return temp;
		}
		else
		{
			T temp = ultimoNodo.darElemento();
			ultimoNodo.darSiguiente().cambiarAnterior(null);
			ultimoNodo = ultimoNodo.darSiguiente();
			size --;
			return temp;
		}
	}
}