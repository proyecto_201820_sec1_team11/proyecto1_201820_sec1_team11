package model.data_structures;

import java.util.Iterator;

public class Cola <T> implements ICola <T>
{
	/**
	 * nodo del elemento que ingres� de primeras
	 */
	private Node<T> primerNodo;

	/**
	 * nodo del elemento que fue el �ltimo en ingresar
	 */
	private Node<T> ultimoNodo;


	private int size;

	public Cola (Node<T> nodoInicial) 
	{
		primerNodo = ultimoNodo = nodoInicial;
		size = 1;
	}
	public Cola(){
	}
	/**
	 * recibe un nodo apartir del cual crea el iterador
	 * @return el iterador de la lista.
	 */
	public ListIterador<T> iterator(Node<T> nodo) 
	{
		return new ListIterador <T> (nodo);
	}

	@Override
	public Iterator<T> iterator() {

		return new Iterador (primerNodo);
	}

	public Node<T> getPrimerNodo()
	{
		return primerNodo;
	}
	public Node<T> getUltimoNodo()
	{
		return ultimoNodo;
	}
	@Override
	public boolean isEmpty() 
	{
		return size==0;
	}
	@Override
	public int size() {
		return size;
	}

	@Override
	public void enqueue(T t)
	{
		if( 0 < size)
		{
			Node<T> nodoAgregandose =  new Node<T>(t);
			ultimoNodo.cambiarSiguiente(nodoAgregandose);
			nodoAgregandose.cambiarAnterior(ultimoNodo);
			ultimoNodo = nodoAgregandose; 
			size ++;
		}
		else
		{
			Node<T> nodoInicial =  new Node<T>(t);
			primerNodo = nodoInicial;
			ultimoNodo = nodoInicial;
			size = 1;
		}
	}

	// retorna null si no hay elementos en la lista
	@Override
	public T dequeue() {
		if(0 < size)
		{
			T resp = primerNodo.darElemento();
			primerNodo = primerNodo.darSiguiente();
			size --;
			if(0 < size)
			{
				primerNodo.darAnterior().cambiarSiguiente(null);
				primerNodo.cambiarAnterior(null);
			}
			return resp;
		}
		return null;
	}
}