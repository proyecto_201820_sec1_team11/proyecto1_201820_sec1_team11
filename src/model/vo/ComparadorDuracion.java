package model.vo;

import java.util.Comparator;

public class ComparadorDuracion implements Comparator <Bike>
{
	@Override
	public int compare(Bike b1, Bike b2)
	{
		if (b1.getTotalDuration() == b2.getTotalDuration())
			return 0;
		else if (b1.getTotalDuration() < b2.getTotalDuration())
			return -1;
		else
			return 1;
	}
}