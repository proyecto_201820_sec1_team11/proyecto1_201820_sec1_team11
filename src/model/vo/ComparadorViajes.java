package model.vo;

import java.util.Comparator;

public class ComparadorViajes implements Comparator <Bike>
{
	@Override
	public int compare(Bike b1, Bike b2)
	{
		if (b1.getTotalTrips() == b2.getTotalTrips())
			return 0;
		else if (b1.getTotalTrips() < b2.getTotalTrips())
			return -1;
		else
			return 1;
	}
}