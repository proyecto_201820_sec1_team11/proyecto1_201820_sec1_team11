package model.vo;
import java.time.LocalDateTime;

public class Station implements Comparable <Station>
{
	private int id;

	private String name;

	private double latitude;

	private double longitude;

	private int dpcapacity;

	private LocalDateTime onlineDate;

	public Station (int id, String nombre, double latitud, double longitud, int capacidad, LocalDateTime fecha) 
	{
		this.id = id;
		name = nombre;
		latitude = latitud;
		longitude = longitud;
		dpcapacity = capacidad;
		onlineDate = fecha;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude()
	{
		return latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude()
	{
		return longitude;
	}

	/**
	 * @return the dpcapacity
	 */
	public int getDpcapacity()
	{
		return dpcapacity;
	}

	/**
	 * @return the online_date
	 */
	public LocalDateTime getOnline_date()
	{
		return onlineDate;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * @param dpcapacity the dpcapacity to set
	 */
	public void setDpcapacity(int dpcapacity)
	{
		this.dpcapacity = dpcapacity;
	}

	/**
	 * @param online_date the online_date to set
	 */
	public void setOnline_date(LocalDateTime online_date)
	{
		onlineDate = online_date;
	}

	@Override
	public int compareTo(Station est)
	{
		if (est.getId() == this.id)
			return 0;
		else if (this.id < est.getId())
			return -1;
		else
			return 1;
	}
}