package model.vo;

import java.util.Comparator;

public class ComparadorDistancia implements Comparator <Bike>
{
	@Override
	public int compare(Bike b1, Bike b2)
	{
		if (b1.getTotalDistance() == b2.getTotalDistance())
			return 0;
		else if (b1.getTotalDistance() < b2.getTotalDistance())
			return -1;
		else
			return 1;
	}
}