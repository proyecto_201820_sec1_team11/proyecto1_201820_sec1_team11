package model.vo;

import java.util.Comparator;

public class ComparadorFechas implements Comparator <Trip>
{
	@Override
	public int compare(Trip t1, Trip t2)
	{
		if (t1.getEndTime() == t2.getEndTime())
			return 0;
		else if (t1.getEndTime().compareTo(t2.getEndTime()) < 0)
			return -1;
		else 
			return 1;
	}
}