package model.vo;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ComparadorEstaciones implements Comparator <Trip>
{
	@Override
	public int compare(Trip t1, Trip t2)
	{
		LocalDateTime d1 = null;
		LocalDateTime d2 = null;

		if(t1.getFromStationName() == null)
			d1 = t1.getEndTime();
		else
			d1 = t1.getStartTime();

		if (t2.getFromStationName() == null)
			d2 = t2.getEndTime();
		else
			d2 = t2.getStartTime();

		if (d1.compareTo(d2) == 0)
			return 0;
		else if (d1.compareTo(d2) < 0)
			return -1;
		else
			return 1;
	}
}