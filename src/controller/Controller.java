package controller;

import model.data_structures.ICola;
import model.data_structures.ILista;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDateTime;

import api.IManager;


public class Controller
{
	private static IManager manager = new Manager();

	public static ICola<Trip> A1(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		return manager.A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
	}

	public static ILista<Bike> A2(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		return manager.A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
	}

	public ILista<Trip> A3(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return manager.A3ViajesPorBicicletaPeriodoTiempo(bikeId, fechaInicial, fechaFinal);
	}

	public ILista<Trip> A4(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return manager.A4ViajesPorEstacionFinal(endStationId, fechaInicial, fechaFinal);
	}

	public ICola<Station> B1(LocalDateTime fechaComienzo) {
		return manager.B1EstacionesPorFechaInicioOperacion(fechaComienzo);
	}

	public ILista<Bike> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return manager.B2BicicletasOrdenadasPorDistancia(fechaInicial, fechaFinal);
	}

	public ILista<Trip> B3(int bikeId, int tiempoMaximo, String genero) {
		return manager.B3ViajesPorBicicletaDuracion(bikeId, tiempoMaximo, genero);
	}

	public ILista<Trip> B4(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return manager.B4ViajesPorEstacionInicial(startStationId, fechaInicial, fechaFinal);
	}
	public static int [] C1cargar(String dataTrips, String dataStations){
		return manager.C1cargar(dataTrips, dataStations);
	}
	public ICola<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		return manager.C2ViajesValidadosBicicleta(bikeId, fechaInicial, fechaFinal);
	}
	public  ILista<Bike> C3BicicletasMasUsadas(int topBicicletas){
		return manager.C3BicicletasMasUsadas(topBicicletas);
	}
	public ILista<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
		return manager.C4ViajesEstacion(StationId, fechaInicial, fechaFinal);
	}
}
