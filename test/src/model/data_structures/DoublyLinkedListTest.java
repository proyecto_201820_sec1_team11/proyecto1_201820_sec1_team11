package model.data_structures;

import org.junit.*;
import static org.junit.Assert.*;

public class DoublyLinkedListTest <E extends Comparable>
{
	private DoublyLinkedList<E> lista;

	public void setupescenario ()
	{
		lista = new DoublyLinkedList <E>();
	}
	public void setupEscenario1 ()
	{
		lista = new DoublyLinkedList<E>();
		for (int i = 0; i < 10; i++)
			lista.add ((E) new Integer (i));
	}

	@Test 
	public void testAdd ()
	{
		setupescenario();
		lista.add ((E) new String ("Prueba"));
		assertEquals ("No se est� agregando correctamente", "Prueba", lista.getElement(0));
		setupEscenario1();
		for (int i = 0; i < 10; i++)
		{
			assertEquals ("No se est� agregando correctamente", i, lista.getElement(i));
		}
	}

	@Test
	public void testAddI ()
	{
		setupEscenario1();
		lista.addAtK(0, (E) new Integer (118));
		assertEquals ("No se est� agregando correctamente", 118, lista.getElement(0));
		lista.addAtK(lista.size(), (E) new Integer (710));
		assertEquals ("No se est� agregando correctamente", 710, lista.getElement(lista.size() - 1));
		lista.addAtK(5, (E) new Integer (208));
		assertEquals ("No se est� agregando correctamente", 208, lista.getElement(5));
	}

	@Test
	public void testSize ()
	{
		setupescenario();
		assertTrue ("No calcula el tama�o de forma correcta", 0 == lista.size());
		setupEscenario1();
		assertTrue ("No calcula el tama�o de forma correcta", 10 == lista.size());
	}

	@Test
	public void testDarNodo ()
	{
		setupEscenario1();
		assertEquals("No retorna el nodo adecuado", 5, lista.getElement(5));
	}
}