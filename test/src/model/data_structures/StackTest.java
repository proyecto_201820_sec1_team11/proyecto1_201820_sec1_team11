package model.data_structures;

import org.junit.*;
import static org.junit.Assert.*;

public class StackTest <T>
{
	private Pila pila;

	public void setupEscenario ()
	{
		pila = new Pila <String> (new String ("a"));
	}

	public void setupEscenario1 ()
	{
		pila = new Pila <String> ();
	}

	@Test
	public void testSize ()
	{
		setupEscenario();
		assertTrue ("El tama�o no es correcto", pila.size() == 1);
		pila.pop();
		assertTrue ("El tama�o no es correcto", pila.size() == 0);
		pila.push(new String ("b"));
		assertTrue ("El tama�o no es correcto", pila.size() == 1);
	}

	@Test
	public void testIsEmpty ()
	{
		setupEscenario1();
		assertTrue ("La pila debe estar vacia", pila.isEmpty());
	}

	@Test
	public void testPush ()
	{
		setupEscenario1();
		pila.push(new String ("b"));
		assertTrue ("El tama�o no es correcto", pila.size() == 1);
		setupEscenario();
		String c = "c";
		pila.push(c);
		assertTrue ("El tama�o no es correcto", pila.size() == 2);
		assertEquals ("No se cambia correctamente la cabeza", c, pila.darUltimo().darElemento());
	}

	@Test
	public void testPop ()
	{
		setupEscenario1();
		String d = "d";
		pila.push(d);
		assertEquals ("No se retorna adecuadamente el ultimo elemento", d, (String) pila.pop());
		assertTrue ("La pila debe estar vacia", pila.isEmpty());
	}
}