package model.data_structures;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.Iterator;

public class IteradorTest <E extends Comparable>
{
	DoublyLinkedList<E> lista;
	Iterator<E> iter;

	public void setupEscenario ()
	{
		lista = new DoublyLinkedList<E>();
		for (int i = 0; i < 10; i++)
			lista.add ((E) new Double (Math.random() * i));
		iter = lista.iterator();
	}

	@Test
	public void hasNextTest ()
	{
		lista = new DoublyLinkedList<E>();
		iter = lista.iterator();
		assertFalse ("No tiene siguiente", iter.hasNext());
		setupEscenario();
		for (int i = 0; i < 10; i++)
			iter.next();
		assertFalse ("No tiene siguiente", iter.hasNext());
	}

	@Test
	public void nextTest ()
	{
		setupEscenario();
		assertEquals("No avanza correctamente", lista.getElement(0), iter.next());
		assertEquals("No avanza correctamente", lista.getElement(1), iter.next());
	}
}