package model.data_structures;


import org.junit.*;
import static org.junit.Assert.*;

/**
 * Clase de prueba para el calculador de impuestos de veh�culos
 */
public class QueueTest 
{
	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------

	private Cola cola;

	// -----------------------------------------------------------------
	// M�todos
	// -----------------------------------------------------------------

	/**
	 * Prepara un escenario con el la cola
	 */
	public void setupEscenario1 ()
	{
		cola = new Cola <String> ();
	}

	/**
	 * Verifica el c�lculo del tama�o de la lista
	 */
	@Test
	public void testSize ()
	{
		setupEscenario1();
		assertTrue ("El tama�o no es correcto", cola.size() == 0);
		cola.enqueue("EstacionX");
		assertTrue ("El tama�o no es correcto", cola.size() == 1);
		cola.dequeue();
		assertTrue ("El tama�o no es correcto", cola.size() == 0);
	}

	/**
	 * Verifica el metodo isEmpty
	 */
	@Test
	public void testIsEmpty ()
	{
		setupEscenario1();
		assertTrue ("La pila debe estar vacia", cola.isEmpty());
	}

	/**
	 * Verifica el m�todo de agregar elementos
	 * 
	 */
	@Test
	public void testEnQueue()
	{
		setupEscenario1();
		cola.enqueue("Estacion1");
		assertTrue ("El tama�o no es correcto", cola.size() == 1);
		cola.enqueue("Estacion2");
		assertTrue ("El tama�o no es correcto", cola.size() == 2);
		assertEquals ("No guarda correctamente la referencia al primer objeto", "Estacion1", cola.getPrimerNodo().darElemento());
		assertEquals ("No guarda correctamente la referencia al segundo objeto", "Estacion2", cola.getUltimoNodo().darElemento());
	}


	/**
	 * Verifica el m�todo de sacar elementos
	 * 
	 */
	@Test
	public void testDeQueue ()
	{
		setupEscenario1();
		cola.enqueue("Estacion1");
		cola.enqueue("Estacion2");
		assertEquals ("No se retorna adecuadamente el primer elemento", "Estacion1",  ""+cola.dequeue());
		cola.dequeue();
		cola.dequeue();

		try
		{
			cola.dequeue();
		}
		catch( Exception e)
		{
			fail("no debe lanzar excepci�n aunque la cola este vac�a");
		}
		assertTrue ("La cola debe estar vacia", cola.isEmpty());
	}

}