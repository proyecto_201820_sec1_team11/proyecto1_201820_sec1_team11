package model.data_structures;

import org.junit.*;
import static org.junit.Assert.*;

public class NodeTest <E>
{
	Node<E> nodo;

	Node<E> siguiente;

	Node<E> anterior;

	@Before
	public void setupEscenario ()
	{
		nodo = new Node<E>((E) new String ("a"));
		anterior = new Node<E>((E) new String ("b"));
		siguiente = new Node<E>((E) new String ("c"));
		nodo.cambiarSiguiente(siguiente);
		nodo.cambiarAnterior(anterior);
		anterior.cambiarSiguiente(nodo);
		siguiente.cambiarAnterior(nodo);
	}

	@Test
	public void darElementoTest ()
	{
		assertEquals("No retorna el elemento esperado", "a", nodo.darElemento());
		assertEquals("No retorna el elemento esperado", "c", siguiente.darElemento());
		assertEquals("No retorna el elemento esperado", "b", anterior.darElemento());	
	}

	@Test
	public void cambiarElementoTest ()
	{
		nodo.cambiarElemento((E) new Integer (420));
		siguiente.cambiarElemento((E) new Double (4.2));
		assertEquals("No se cambi� el elemento correctamnte", (Integer) 420, nodo.darElemento());
		assertEquals("No se cambi� el elemento correctamnte", (Double) 4.2, siguiente.darElemento());
	}

	@Test
	public void darSiguienteTest ()
	{
		assertEquals("El siguiente no es el esperado", siguiente, nodo.darSiguiente());
		assertEquals("El siguiente no es el esperado", nodo, anterior.darSiguiente());
	}

	@Test
	public void darAnteriorTest ()
	{
		assertEquals("El anterior no es el esperado", anterior, nodo.darAnterior());
		assertEquals("El anterior no es el esperado", nodo, siguiente.darAnterior());
	}

	@Test
	public void cambiarSiguienteTest ()
	{
		siguiente.cambiarSiguiente(nodo);
		nodo.cambiarSiguiente(anterior);
		assertEquals("No se cambi� el elemento correctamnte", nodo, siguiente.darSiguiente());
		assertEquals("No se cambi� el elemento correctamnte", anterior, nodo.darSiguiente());
	}

	@Test
	public void cambiarAnteriorTest ()
	{
		anterior.cambiarAnterior(siguiente);
		siguiente.cambiarAnterior(anterior);
		assertEquals("No se cambi� el elemento correctamnte", siguiente, anterior.darAnterior());
		assertEquals("No se cambi� el elemento correctamnte", anterior, siguiente.darAnterior());
	}
}